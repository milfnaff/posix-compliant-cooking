# Install

## Target machine

Check boot mode

```
# ls /sys/firmware/efi/efivars
```

Set root password
```
# passwd
```

Configure and start the sshd service

```
# /etc/ssh/sshd_config
PermitRootLogin yes
```

```
# systemctl start sshd.service
```

Wireless
```
# iwctl
[iwd]# device list
[iwd]# station DEVICE scan
[iwd]# station DEVICE get-networks
[iwd]# station DEVICE connect SSID
```

Get the local IP address

```
# ip a
```

## Local machine

Connect to the target machine
```
# ssh root@IPaddress
```

Check the internet connection

```
# ping archlinux.org
```

Update the system clock
```
# timedatectl set-ntp true
```

Partition the disk
```
# lsblk
# gdisk /dev/nvme0n1
    1. ef00 1024MB
    2. 8e00 remaining space
```

Setup LUKS
```
# cryptsetup luksFormat —y /dev/nvme0n1p2
# cryptsetup open --type luks /dev/nvme0n1p2 lvm
```

Setup LVM
```
# pvcreate /dev/mapper/lvm
# vgcreate system /dev/mapper/lvm
# lvcreate -L 16G -n swap system
# lvcreate -l 100%FREE -n root system
```

Format the partitions
```
# mkfs.fat -F32 /dev/nvme0n1p1
# mkfs.ext4 /dev/mapper/system-root
# mkswap /dev/mapper/system-swap
# swapon /dev/mapper/system-swap
```

Mount the partitions
```
# mount /dev/mapper/system-root /mnt
# mkdir -p /mnt/boot
# mount /dev/nvme0n1p1 /mnt/boot
# lsblk -f /dev/nvme0n1p1
```

Update mirrors
```
# reflector --country COUNTRY --latest 5 --protocol http --protocol https --sort rate --save /etc/pacman.d/mirrorlist
```

Install base packages
```
# pacstrap -i /mnt base base-devel linux linux-lts linux-firmware lvm2 vim iwd openssh man-pages man-db
```

Generate fstab
```
# genfstab -U /mnt >> /mnt/etc/fstab
```

### Chroot
```
# arch-chroot /mnt /bin/bash
```

Timezone configuration
```
# ln -sf /usr/share/zoneinfo/Region/City /etc/localtime
```

Generate /etc/adjtime
```
# hwclock --systohc
```

Edit /etc/locale.gen and generate locales
```
# locale-gen
```

Set the LANG variable
```
# /etc/locale.conf
LANG=en_US.UTF-8
```

### Network configuration

https://wiki.archlinux.org/title/Systemd-networkd

```
# echo HOSTNAME > /etc/hostname
# systemctl enable systemd-networkd.service
```

Wired DHCP
```
# /etc/systemd/network/20-wired.network
[Match]
Name=eth0

[Network]
DHCP=yes
```

Wireless DHCP
```
# /etc/systemd/network/25-wireless.network
[Match]
Name=wlan0

[Network]
DHCP=yes
IgnoreCarrierLoss=3s
```

Edit /etc/mkinitcpio.conf and add `encrypt lvm2 resume` between `block` and `filesystem` hooks.
```
# mkinitcpio -P
```

Set root password
```
# passwd
```

Install microcode updates `amd-ucode` or `intel-ucode`

Install bootloader
```
# bootctl install
```

```
# /boot/loader/loader.conf
timeout 3
default arch
editor no
```

```
# /boot/loader/entries/arch.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /boot/<CPU-MANUFACTURER>-ucode.img
initrd  /initramfs-linux.img
options cryptdevice=UUID=<UUID>:system:allow-discards root=/dev/mapper/system-root resume=/dev/mapper/system-swap quiet rw splash

# /boot/loader/entries/arch-lts.conf
title   Arch Linux LTS
linux   /vmlinuz-linux-lts
initrd  /boot/<CPU-MANUFACTURER>-ucode.img
initrd  /initramfs-linux-lts.img
options cryptdevice=UUID=<UUID>:system:allow-discards root=/dev/mapper/system-root resume=/dev/mapper/system-swap quiet rw splash
```

Reboot
```
# exit
# umount -R /mnt
# reboot
```

# Post-install

## Network configuration

- DNS https://wiki.archlinux.org/title/Systemd-resolved
- Firewall https://wiki.archlinux.org/title/Nftables
- Clock synchronization https://wiki.archlinux.org/title/Systemd-timesyncd

## Hardware configuration

### SSD

- TRIM https://wiki.archlinux.org/title/Solid_state_drive#TRIM

### Laptop

- Charge threshold https://wiki.archlinux.org/title/Laptop/ASUS#Battery_charge_threshold
- Hibernate on low charge
https://wiki.archlinux.org/title/Laptop#Hibernate_on_low_battery_level
- Extra keys https://wiki.archlinux.org/title/Extra_keyboard_keys
- Backlight https://wiki.archlinux.org/title/Backlight
- Touchpad https://wiki.archlinux.org/title/Libinput
- Webcam https://wiki.archlinux.org/title/Webcam_setup
- DPMS https://wiki.archlinux.org/title/Display_Power_Management_Signaling

## User configuration

### User

```
# useradd -mG wheel $USERNAME
# passwd $USERNAME
# EDITOR=vim visudo
```

### Shell

```
# pacman -Syu dash zsh zsh-completions zsh-syntax-highlighting zsh-autosuggestions
# usermod -s /usr/bin/zsh $USERNAME
```

```
# /usr/share/libalpm/hooks/dashbinsh.hook
[Trigger]
Type = Package
Operation = Install
Operation = Upgrade
Target = bash

[Action]
Description = Re-pointing /bin/sh symlink to dash...
When = PostTransaction
Exec = /usr/bin/ln -sfT dash /usr/bin/sh
Depends = dash
```

Login as `$USERNAME`

```
# ~/.zshrc
autoload -Uz compinit promptinit

compinit
zstyle ':completion:*' menu select
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

promptinit
prompt redhat
```

### X

```
# pacman -Syu xorg xorg-xinit
```

```
# ~/.xinitrc
exec dwm
```
